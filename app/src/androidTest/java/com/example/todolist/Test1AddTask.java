package com.example.todolist;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.test.ActivityInstrumentationTestCase2;
import com.robotium.solo.Solo;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class Test1AddTask extends ActivityInstrumentationTestCase2<ToDoListActivity> {

    private Solo solo;

    public Test1AddTask() {
        super(ToDoListActivity.class);
    }

    @Test
    public void useAppContext() {

        // Injecting the Instrumentation instance is required
        // for your test to run with AndroidJUnitRunner.
        injectInstrumentation(InstrumentationRegistry.getInstrumentation());
        solo = new Solo(getInstrumentation(), getActivity());

        // ============= Section One ===============
        // Wait for activity: 'com.example.todolist.ToDoListActivity'
        assertTrue("SubmitTest failed:" + "Section One:" + "ToDoListActivity did not load correctly.", solo.waitForActivity(ToDoListActivity.class, 2000));

        // Click on action bar item to delete all items
        solo.clickOnActionBarItem(0x1);

        // Click on Add New ToDo Item
        solo.clickOnView(solo.getView(R.id.footerView));

        // Hide the soft keyboard
        solo.hideSoftKeyboard();

        // Enter the text: 'Simple Task'
        solo.clearEditText((android.widget.EditText) solo.getView(R.id.title));
        solo.enterText((android.widget.EditText) solo.getView(R.id.title), "Simple Task");

        // Hide the soft keyboard
        solo.hideSoftKeyboard();
        // Click on Done:
        solo.clickOnView(solo.getView(R.id.statusDone));
        // Click on Low
        solo.clickOnView(solo.getView(R.id.lowPriority));
        // Click on Submit
        solo.clickOnView(solo.getView(R.id.submitButton));

        // ================= Section Two ================
        // Wait for activity: 'com.example.todolist.ToDoListActivity'
        assertTrue("SubmitTest failed:" + "Section Two:" + "ToDoListActivity did not load correctly after pressing submit.", solo.waitForActivity(ToDoListActivity.class));
        assertTrue("SubmitTest failed:" + "Section Two:" + "Title was not correctly entered in the ToDoManager", solo.searchText("Simple Task"));
        assertTrue("SubmitTest failed:" + "Section Two:" + "Priority was not correctly entered in the ToDoManager", solo.searchText("LOW"));
        assertTrue("SubmitTest failed:" + "Section Two:" + "Did not correctly set completion status.", solo.isCheckBoxChecked(0));

    }


}
