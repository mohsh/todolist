package com.example.todolist;

import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.test.ActivityInstrumentationTestCase2;

import com.robotium.solo.Solo;

import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class Test3CancelTest extends ActivityInstrumentationTestCase2<ToDoListActivity> {

    private Solo solo;

    public Test3CancelTest() {
        super(ToDoListActivity.class);
    }

    @Test
    public void useAppContext() {

        // Injecting the Instrumentation instance is required
        // for your test to run with AndroidJUnitRunner.
        injectInstrumentation(InstrumentationRegistry.getInstrumentation());

        solo = new Solo(getInstrumentation(), getActivity());

        // Wait for activity: 'com.example.todolist.ToDoListActivity'
        assertTrue("CancelTest failed:" + "Section One:" + "ToDoListActivity did not load correctly.",
                solo.waitForActivity(
                        com.example.todolist.ToDoListActivity.class, 2000));

        // Click on action bar item
        solo.clickOnActionBarItem(0x1);

        // Click on Add New ToDo Item
        solo.clickOnView(solo.getView(com.example.todolist.R.id.footerView));

        // Wait for activity: 'com.example.todolist.AddToDoActivity'
        assertTrue("CancelTest failed:" +
                        "Section One:" +
                        "AddToDoActivity did not load correctly.",
                solo.waitForActivity(com.example.todolist.AddToDoActivity.class));

        // Hide the soft keyboard
        solo.hideSoftKeyboard();
        // Enter the text: 'Simple Task 1'
        solo.clearEditText((android.widget.EditText) solo
                .getView(com.example.todolist.R.id.title));
        solo.enterText((android.widget.EditText) solo
                .getView(com.example.todolist.R.id.title), "Simple Task 1");
        // Hide the soft keyboard
        solo.hideSoftKeyboard();
        // Click on Done:
        solo.clickOnView(solo.getView(com.example.todolist.R.id.statusDone));
        // Click on High
        solo.clickOnView(solo
                .getView(com.example.todolist.R.id.highPriority));

        // Click on Cancel
        solo.clickOnView(solo
                .getView(com.example.todolist.R.id.cancelButton));

        // Click on Add New ToDo Item
        solo.clickOnView(solo.getView(com.example.todolist.R.id.footerView));
        // Hide the soft keyboard
        solo.hideSoftKeyboard();
        // Enter the text: 'Simple Task 2'
        solo.clearEditText((android.widget.EditText) solo
                .getView(com.example.todolist.R.id.title));
        solo.enterText((android.widget.EditText) solo
                .getView(com.example.todolist.R.id.title), "Simple Task 2");

        // Hide the soft keyboard
        solo.hideSoftKeyboard();
        // Click on Done:
        solo.clickOnView(solo.getView(com.example.todolist.R.id.statusDone));
        // Click on Low
        solo.clickOnView(solo.getView(com.example.todolist.R.id.lowPriority));

        // Click on Submit
        solo.clickOnView(solo
                .getView(com.example.todolist.R.id.submitButton));

        // ================ Section Two ===================
        // Wait for activity: 'com.example.todolist.ToDoListActivity'
        assertTrue("CancelTest failed:" +
                        "Section Two:" +
                        "ToDoListActivity did not load correctly.",
                solo.waitForActivity(com.example.todolist.ToDoListActivity.class));

        assertFalse("CancelTest failed:" +
                        "Section Two:" +
                        "Did not correctly cancel the creation of a ToDo Task.",
                solo.searchText("Simple Task 1"));

        assertTrue("CancelTest failed:" +
                        "Section Two:" +
                        "Did not correctly set title of ToDo Task following cancel.",
                solo.searchText("Simple Task 2"));
        assertTrue("CancelTest failed:" +
                        "Section Two:" +
                        "Did not correctly set priority of ToDo Task following cancel.",
                solo.searchText("LOW"));


    }


}
