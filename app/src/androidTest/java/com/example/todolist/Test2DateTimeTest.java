package com.example.todolist;

import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.test.ActivityInstrumentationTestCase2;

import com.robotium.solo.Solo;

import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class Test2DateTimeTest extends ActivityInstrumentationTestCase2<ToDoListActivity> {

    private Solo solo;

    public Test2DateTimeTest() {
        super(ToDoListActivity.class);
    }

    @Test
    public void useAppContext() {

        // Injecting the Instrumentation instance is required
        // for your test to run with AndroidJUnitRunner.
        injectInstrumentation(InstrumentationRegistry.getInstrumentation());

        solo = new Solo(getInstrumentation(), getActivity());

        // ============== Section One ================
        // Wait for activity: 'com.example.todolist.ToDoListActivity'
        assertTrue("DateTimeTest failed:" + "Section One:" + "ToDoListActivity did not correctly load.", solo.waitForActivity(com.example.todolist.ToDoListActivity.class, 2000));

        // Click on action bar item to delete all items
        solo.clickOnActionBarItem(0x1);

        // Click on Add New ToDo Item
        solo.clickOnView(solo.getView(com.example.todolist.R.id.footerView));

        // Wait for activity: 'com.example.todolist.AddToDoActivity'
        assertTrue("DateTimeTest failed:" + "Section One:" + "AddToDoActivity did not correctly load.", solo.waitForActivity(com.example.todolist.AddToDoActivity.class));

        // Hide the soft keyboard
        solo.hideSoftKeyboard();

        // Enter the text: 'Simple Task'
        solo.clearEditText((android.widget.EditText) solo.getView(com.example.todolist.R.id.title));
        solo.enterText((android.widget.EditText) solo.getView(com.example.todolist.R.id.title), "Simple Task");

        // Hide the soft keyboard
        solo.hideSoftKeyboard();

        // Click on Done:
        solo.clickOnView(solo.getView(com.example.todolist.R.id.statusDone));
        // Click on Low
        solo.clickOnView(solo.getView(com.example.todolist.R.id.lowPriority));
        // Click on Choose Date
        solo.clickOnView(solo.getView(com.example.todolist.R.id.date_picker_button));

        // Wait for dialog
        solo.waitForDialogToOpen(10000);
        // Enter the text: 'Feb'
        solo.clearEditText((android.widget.EditText) solo.getView("numberpicker_input"));
        solo.enterText((android.widget.EditText) solo.getView("numberpicker_input"),"Feb");
        // Enter the text: '28'
        solo.clearEditText((android.widget.EditText) solo.getView("numberpicker_input", 1));
        solo.enterText((android.widget.EditText) solo.getView("numberpicker_input", 1),"28");
        // Enter the text: '2014'
        solo.clearEditText((android.widget.EditText) solo.getView("numberpicker_input", 2));
        solo.enterText((android.widget.EditText) solo.getView("numberpicker_input", 2),"2014");

        // Really set the date
        solo.setDatePicker(0, 2014, 1, 28);

        // Click on Done
        solo.clickOnView(solo.getView(android.R.id.button1));

        // Click on Choose Time
        solo.clickOnView(solo.getView(com.example.todolist.R.id.time_picker_button));
        // Wait for dialog
        solo.waitForDialogToOpen(10000);
        // Enter the text: '9'
        solo.clearEditText((android.widget.EditText) solo.getView("numberpicker_input"));
        solo.enterText((android.widget.EditText) solo.getView("numberpicker_input"),"9");
        // Enter the text: '19'
        solo.clearEditText((android.widget.EditText) solo.getView("numberpicker_input", 1));
        solo.enterText((android.widget.EditText) solo.getView("numberpicker_input", 1),"19");

        // Really set the time
        solo.setTimePicker(0, 9, 19);

        // Click on Done
        solo.clickOnView(solo.getView(android.R.id.button1));

        // Click on Submit
        solo.clickOnView(solo.getView(com.example.todolist.R.id.submitButton));

        // Wait for activity: 'com.example.todolist.ToDoListActivity'
        assertTrue("DateTimeTest failed:" + "Section One:" + "ToDoListActivity did not load correctly", solo.waitForActivity(com.example.todolist.ToDoListActivity.class, 2000));

        // ============== Section Two =============
        // Makes sure the title was changed correctly
        assertTrue("DateTimeTest failed:" + "Section Two:" + "Did not modify title correctly",solo.searchText("Simple Task"));

        // Checks to see if the status was changed correctly
        assertTrue("DateTimeTest failed:" + "Section Two:" + "Did not change status correctly",solo.isCheckBoxChecked(0));

        // Checks to make sure the priority was correctly set
        assertTrue("DateTimeTest failed:" + "Section Two:" + "Did not correctly set priority",solo.searchText("LOW"));

        // Checks to make sure the Date was correctly set
        assertTrue("DateTimeTest failed:" + "Section Two:" + "Did not correctly set the date",solo.searchText("2014-02-28"));

        // Checks to make sure the Time was correctly set
        assertTrue("DateTimeTest failed:" + "Section Two:" + "Did not correctly set the time",solo.searchText("09:19:00"));

    }


}
